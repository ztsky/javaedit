import io.restassured.http.ContentType;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;
import org.json.JSONArray;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;

import static io.restassured.RestAssured.get;
import static io.restassured.RestAssured.given;

public class TestAPI {

    @Test
    public void testGetRequest() {

        String url = "http://restcountries.eu/rest/v1/name/ukraine";

        ResponseBody response = get(url);


        JSONArray responseBody = new JSONArray(response.asString());

        String actualVal = responseBody.getJSONObject(0).getString("capital");

        Assert.assertEquals(actualVal, "Kiev");


    }


    @Test
    public void testPostRequest() {

        RequestBody body = new RequestBody("Roma", "Onyshkiv", 31);

        Response response = given().contentType(ContentType.JSON).body(body).when().post("http://localhost:8080/create");


        int status = response.getStatusCode();

        Assert.assertEquals(status, 201);


    }


    @Test
    public void negativePost() {

        RequestBody requestBody = new RequestBody();
        requestBody.setFirstName("Test");

        Response response = given().contentType(ContentType.JSON).body(requestBody).when().post("http://localhost:8080/create");

        int status = response.getStatusCode();

        Assert.assertNotEquals(status, 201);


    }

}
